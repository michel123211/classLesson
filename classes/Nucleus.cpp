#include "Nucleus.h"
// also setter
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	set_start(start);
	set_end(end);
	set_complementary(on_complementary_dna_strand);
}
// setters
void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}
void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}
void Gene::set_complementary(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}
// getters
unsigned int Gene::get_start() const
{
	return this->_end;
}
unsigned int Gene::get_end() const
{
	return this->_start;
}
bool Gene::is_on_compementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}


int Nucleus::init(const std::string dna_sequence)
{
	for (int i = 0; i < dna_sequence.size(); i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'T' && dna_sequence[i] != 'G' && dna_sequence[i] != 'C')
		{
			std::cerr << "This input is invalid" << endl;
			_exit(1);
		}
	}
	this->_DNA_strand = dna_sequence;
	for (int i = 0; i < dna_sequence.size(); i++)
	{
		if (dna_sequence[i] == 'A')
		{
			this->_complementary_dna_strand = this->_complementary_dna_strand + 'T';
		}
		else if (dna_sequence[i] == 'T')
		{
			this->_complementary_dna_strand = this->_complementary_dna_strand + 'A';
		}
		else if (dna_sequence[i] == 'G')
		{
			this->_complementary_dna_strand = this->_complementary_dna_strand + 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			this->_complementary_dna_strand = this->_complementary_dna_strand + 'G';
		}
	}


	return 0;
}
std::string Nucleus::get_dna() const
{
	return this->_DNA_strand;
}
std::string Nucleus::get_complementary_dna() const
{
	return this->_complementary_dna_strand;
}

std::string get_RNA_transcript(const Gene& gene)
{
	Nucleus _nuclues;
	std::string _start_to_end_rna;
	std::string str1;
	unsigned int start = gene.get_start();
	unsigned int end = gene.get_end();
	bool is_complementary = gene.is_on_compementary_dna_strand();

	if (is_complementary)
	{
		str1 = _nuclues.get_complementary_dna();
	}
	else
	{
		str1 = _nuclues.get_dna();
	}
	_start_to_end_rna = str1.substr(start, end);

	for (int i = 0; i < _start_to_end_rna.size(); i++)
	{
		if (_start_to_end_rna[i] == 'T')
		{
			_start_to_end_rna[i] = 'U';
		}
	}
	return _start_to_end_rna;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string str1 = this->get_dna();
	std::string rev;
	for (int i = str1.size(); i >= 0; i--)
	{
		rev = rev.append(1, str1[i]);
	}
	return rev;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string dna_strand = this->get_dna();
	unsigned int count = 0;
	size_t counter = 0;
	while (counter != string::npos)
	{
		size_t i = counter + codon.length();
		counter = dna_strand.find(codon, i);
		count++;
	}
	return count;
}

