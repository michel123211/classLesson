#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "Prote.h"
#include "AminoAcid.h"
#include "Ribosome.h"

using namespace std;


class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_compementary_dna_strand() const;
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
	void set_start(const unsigned int);
	void set_end(const unsigned int);
	void set_complementary(const bool);
};

class Nucleus
{
public:
	int init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_dna() const;
	std::string get_complementary_dna() const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	std::string _DNA_strand = "";
	std::string _complementary_dna_strand = "";
};

